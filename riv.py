from random import choice
from turtle import *
from freegames import floor, vector
import pygame
from PIL import Image

youlose = Image.open("you_lose.png");
youwin = Image.open("you_win.jpg");
state = {'score': 0}
path1 = Turtle(visible=False)
path2 = Turtle(visible=False)
writer = Turtle(visible=False)
aim = vector(5, 0)
pacman = vector(-20, -180)
ghosts = [
    [vector(-80, -120), vector(-5, 0)],
    [vector(60, -80), vector(5, 0)],
    [vector(-100, -100), vector(5, 0)],
    [vector(80, -140), vector(-5, 0)],

    [vector(-40, -20), vector(-5, 0)],
    [vector(60, -40), vector(5, 0)],
    [vector(-180, 0), vector(5, 0)],
    [vector(120, 20), vector(-5, 0)],

    [vector(-80, 60), vector(-5, 0)],
    [vector(60, 80), vector(5, 0)],
    [vector(-100, 100), vector(5, 0)],
    [vector(80, 120), vector(-5, 0)],

    [vector(80, -60), vector(0, 0)],
    [vector(60, 40), vector(0, 0)],
    [vector(-100, -60), vector(0, 0)],
    [vector(-80, 40), vector(0, 0)],
    [vector(0, -60), vector(0, 0)],
    [vector(0, 40), vector(0, 0)],
    [vector(-160, -60), vector(0, 0)],
    [vector(-180, 40), vector(0, 0)],

    [vector(40, 140), vector(0, 0)],
    [vector(-60, 140), vector(0, 0)],
    [vector(-120, 140), vector(0, 0)],
    [vector(100, 140), vector(0, 0)],

]
tiles = [
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0, 0,
    0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
    0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0,
    0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0,
    0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0,
    0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0,
    0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
    0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0,
    0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0,
    0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0,
    0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0,
    0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
    0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0,
    0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0,
    0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0,
    0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0,
    0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
    0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
]

def square(x, y):
    "Draw square using path at (x, y)."
    path1.up()
    path1.goto(x, y)
    path1.down()
    path1.begin_fill()

    path2.up()
    path2.goto(x, y)
    path2.down()
    path2.begin_fill()

    for count in range(4):
        path2.forward(20)
        path2.left(90)

    for count in range(4):
        path1.forward(20)
        path1.left(90)

    path1.end_fill()
    path2.end_fill()

def offset(point):
    "Return offset of point in tiles."
    x = (floor(point.x, 20) + 200) / 20
    y = (180 - floor(point.y, 20)) / 20
    index = int(x + y * 20)
    return index

def valid(point):
    "Return True if point is valid in tiles."
    index = offset(point)

    if tiles[index] == 0:
        return False

    index = offset(point + 19)

    if tiles[index] == 0:
        return False

    return point.x % 20 == 0 or point.y % 20 == 0

def world():
    "Draw world using path."
    bgcolor('black')
    path1.color('red')
    path2.color('blue')

    for index in range(len(tiles)):
        tile = tiles[index]

        if tile > 0:
            x = (index % 20) * 20 - 200
            y = 180 - (index // 20) * 20
            square(x, y)

            if tile == 1:
                path1.up()
                path1.goto(x + 10, y + 10)
                path1.dot(10, 'green')
            if tile == 2:
                path2.up()
                path2.goto(x + 10, y + 10)
                path2.dot(3, 'black')
            if tile == 3:
                path2.up()
                path2.goto(x + 10, y + 10)
                path2.dot(10, 'orange')


def move():
    "Move pacman and all ghosts."
    writer.undo()
    writer.write(state['score'])

    clear()

    if valid(pacman + aim):
        pacman.move(aim)

    index = offset(pacman)

    if tiles[index] == 1:
        tiles[index] = 3
        state['score'] += 1
        x = (index % 20) * 20 - 200
        y = 180 - (index // 20) * 20
        square(x, y)

    up()
    goto(pacman.x + 10, pacman.y + 10)
    dot(20, 'yellow')

    for point, course in ghosts:
        if valid(point + course):
            point.move(course)
        else:
            options = [
                vector(5, 0),
                vector(-5, 0),
                #vector(0, 5),
                #vector(0, -5),
            ]
            plan = choice(options)
            course.x = plan.x
            course.y = plan.y

        up()
        goto(point.x + 10, point.y + 10)
        dot(20, 'purple')

    update()


    for point, course in ghosts:
        if abs(pacman - point) < 18:
            youlose.show();
            return

        if (pacman.y == 160):
            youwin.show();
            return

    ontimer(move, 100)

def change(x, y):
    "Change pacman aim if valid."
    if valid(pacman + vector(x, y)):
        aim.x = x
        aim.y = y

setup(420, 420, 370, 0)
hideturtle()
tracer(False)
writer.goto(180, 180)
writer.color('white')
writer.write(state['score'])
listen()
onkey(lambda: change(5, 0), 'Right')
onkey(lambda: change(-5, 0), 'Left')
onkey(lambda: change(0, 5), 'Up')
onkey(lambda: change(0, -5), 'Down')
world()
move()
done()
